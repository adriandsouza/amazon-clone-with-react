import React from 'react'
import { useStateValue, StateProvider } from './StateProvider';
import "./Checkout.css";
import CheckoutProduct from "./CheckoutProduct.js";
import Subtotal from './Subtotal';
function Checkout() {
    const[{basket},dispatch]=useStateValue();
    return (
        <div className="checkout">
            <div className="checkout__left">
            <img className="checkout__ad" src="https://images-na.ssl-images-amazon.com/images/G/01/credit/img16/CCMP/newstorefront/YACC-desktop-nonprime-banner3.png" alt="">
            </img>
            {basket?.length === 0 ? (
                <div>
                    <h2 className="checkout___title">Your shopping basket is empty</h2>
                    <img className="checkout___image" src="https://www.iconspng.com/uploads/sad-dog.png"/>
                </div>
            ):(
                <div>
                    <h2 className="checkout___title">
                        Your shopping Cart
                        {basket?.map(item =>(
                        <CheckoutProduct
                        id={item.id}
                        title={item.title}
                        image={item.image}
                        price={item.price}
                        rating={item.rating}
                        />
                        ))}
                    </h2>
                </div>

            )}
            </div>
            {basket.length>0 &&(
                <div className="checkout__right">
                 <Subtotal/>
                </div>
            )}
        </div>
    )
}

export default Checkout
