import React, {useState} from 'react';
import "./Login.css";
import { Link, useHistory } from 'react-router-dom';
import { auth } from './firebase';
function Login() {
    const history=useHistory();

    const [email,setEmail]=useState('');

    const[password,setPassword]=useState('');

    const login=(event)=>{
        event.preventDefault(); //This stops the refresh
        auth.signInWithEmailAndPassword(email,password)
         .then((auth)=>{
             history.push('/');

        })
        .catch((e)=> alert(e.message));      
    };

    const register=(event)=>{
        event.preventDefault(); //This stops the refresh
        auth.createUserWithEmailAndPassword(email,password)
        .then(auth=>{
            history.push('/');
        })
        .catch((e)=>alert(e.message));
    };
    return (
        <div className="login">
            <Link to="/">
            <img className="login__logo" src="https://1000logos.net/wp-content/uploads/2016/10/Amazon-Logo.png" alt=""/>
            </Link>
            <div className="login__container">
            <form>            
            <h5 className="login__cred">Email id</h5>
            <input value={email} onChange={event=> setEmail(event.target.value)} className="login__text" type="email"></input>
            <h5 className="login__cred">Password</h5>
            <input value={password} onChange={event=> setPassword(event.target.value)} className="login__text" type="password"></input>
            </form>
            <button onClick={login} className="login__button">Login</button>
            <p>By continuing, you agree to Amazon's Conditions of Use and Privacy Notice.
            </p>
            <button onClick={register} className="login__create" > Create your Amazon Account</button>
            </div>
        </div>
            
        
    )
}

export default Login
