import firebase from "firebase";

const firebaseApp=firebase.initializeApp({
        apiKey: "AIzaSyC1ciiE7UIWxkhjdEl_JdciGWNx_EzUVU8",
        authDomain: "clone-93167.firebaseapp.com",
        databaseURL: "https://clone-93167.firebaseio.com",
        projectId: "clone-93167",
        storageBucket: "clone-93167.appspot.com",
        messagingSenderId: "645091405295",
        appId: "1:645091405295:web:92f9cd2dc77017d7fb9a1a",
        measurementId: "G-TYK8TWZ9E8",
    

});

const db=firebaseApp.firestore();
const auth=firebase.auth();

export{db,auth};