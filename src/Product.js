import React from 'react';
import "./Product.css";
import"./CheckoutProduct";
import { useStateValue } from './StateProvider';
function Product({id,title,price,rating, image}) {
    const[{basket},dispatch]=useStateValue();

    const addToBasket=()=>{
        
        dispatch({
            type: 'ADD_TO_BASKET',
            item: {
                id: id,
                title: title,
                price:price,
                rating:rating,
                image:image,
    
            }
        })
    };
    return (
        <div className="product">
            
            <p>{title}</p>
            <p className="product__price">
            <small>₹</small>
            <strong>{price}</strong>
            </p>
            <div className="product__rating">
                {Array(rating)
                .fill()
                .map((_)=>(
                    <p>⭐</p>
                ))}
                </div>     
            
            <img src={image} alt=""/>
            <button onClick={addToBasket}>Add to Basket</button>
              
            
        </div>
    );
}

export default Product
