import React from 'react'
import "./Header.css"
import {Link} from 'react-router-dom';
import SearchIcon from '@material-ui/icons/Search';
import AddShoppingCartOutlinedIcon from '@material-ui/icons/AddShoppingCartOutlined';
import { useStateValue } from './StateProvider';
import {auth} from './firebase';

function Header() {

    const [{basket,user}] = useStateValue();
 
    console.log(basket,user);
    const login = () =>{
       if(user){
          auth.signOut();
       }
    }
    
    return( <nav className="header">
            {/*Amazon logo*/}
             <Link to="/">
            <img className="header__logo" 
            src="https://www.pinclipart.com/picdir/big/358-3584545_amazon-web-services-logo-png-transparent-svg-vector.png"></img>
             </Link>

            {/*Search bar and search Icon*/}
            <div className="header__top">
            <input type="text" className="header__searchInput"/>
            <SearchIcon className="header__searchIcon"> </SearchIcon>
            </div>
            
            {/*other icons on nav bar*/}
             <div className="header__nav">
                 <Link to={!user && "/login"} className="header__link">
                 <div onClick={login} className="header__option">
                 <span className="header__optionLineOne"> Hello {user?.email}</span>
                 <span className="header__optionLineTwo"> {user ? 'Sign Out' : "Sign In"}</span>
                 </div>
                 </Link>

                 <Link to="/" className="header__link">
                 <div className="header__option">
                 
                 <span className="header__optionLineOne"> Returns</span>
                 <span className="header__optionLineTwo"> & Orders</span>
                 </div>
                 </Link>

                 <Link to="/" className="header__link">
                 <div className="header__option">
                 <span className="header__optionLineOne"> Your</span>
                 <span className="header__optionLineTwo"> Prime</span>
                 </div>
                 </Link>
                  

             
                 <Link to="/checkout" className="header_link">
                 <div className="header__cartInput">
                 <AddShoppingCartOutlinedIcon className="header__cart" /> 
                <span className="header__optionLineTwo">{basket?.length}</span>
                 
                 </div>
                 </Link>
             

             </div>
        </nav>
    )
}

export default Header
