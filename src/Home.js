import React from 'react';
import "./Home.css";
import Product from './Product';
import { Tab } from '@material-ui/core';
function Home() {
    return (
        <div className="home">
            <img className="home__image" src="https://d13ezvd6yrslxm.cloudfront.net/wp/wp-content/images/banjaminbuttonart.jpg"></img>
            <div className="home__product">
            <Product
                id="12345"
                title="Ikigai"
                price={100}
                rating={5}
                image="https://images-na.ssl-images-amazon.com/images/I/814L+vq01mL.jpg">
            </Product>
            <Product 
                id="12344"
                title="Think and Grow Rich"
                price={150}
                rating={5}
                image="https://images-na.ssl-images-amazon.com/images/I/713rQq1bF6L.jpg">
            </Product>
            </div>
            <div className="home__product">
            <Product
                id="12343"
                title="Philips HD7431/20 760-Watt Coffee Maker (Black)"
                price={2999}
                rating={5}
                image="https://images-na.ssl-images-amazon.com/images/I/71sgJeU0kkL._SL1500_.jpg">
            </Product>
            <Product 
                id="12342"
                title="Circle Gaming APFC Modular 550 Watt 80 Plus Power Supply"
                price={6750}
                rating={5}
                image="https://images-na.ssl-images-amazon.com/images/I/61gW0pMLuQL._SL1000_.jpg">
            </Product>
            <Product
                id="12341"
                title="New Apple iPad Pro (11-inch, Wi-Fi, 128GB) - Silver(2nd Gen)"
                price={4650}
                rating={4}
                image="https://images-na.ssl-images-amazon.com/images/I/815a%2BXjrgvL._SL1500_.jpg">
            </Product>
            
            </div>
            <div className="home__product">
            
            <iframe className="home__trailer" src="https://www.youtube.com/embed/GuS5BZTUVJs"width="100%" height="350px"></iframe>
            <Product
                id="12340"
                title="OnePlus Y Series 80 cm (32 inches) HD Ready LED Smart Android TV 32Y1 (Black) (2020 Model)"
                price={12999}
                rating={4}
                image="https://images-na.ssl-images-amazon.com/images/I/817gj7pfUzL._SL1500_.jpg">
            </Product>
            
            </div>
        </div>
    )
}

export default Home
